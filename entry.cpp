#ifndef ST
#include <thread>
#include <mutex>
#endif // ST

#include "avx512.h"
#include "seq.h"
#include "avx2.h"
#include "e.hpp"

void run_sum(const input_t<sum_func_t> &input, const test_vec_t &test_input, size_t max_size)
{
    if (input.vec.size() == 0)
    {
        return;
    }

    res_vec_t vec;
    double_mtx_vec_t res(max_size * max_size);

    for (auto &t : test_input)
    {
        if (t.size > max_size)
        {
            break;
        }

        auto l_data = t.mtx1;
        auto r_data = t.mtx2;
        auto dest_data = res.data();

        for (auto &iter : input.vec)
        {
            auto run_func = [l_data, r_data, dest_data, iter](size_t i)
            {
                iter.second(l_data, r_data, dest_data, i, i);
            };
            get_result(vec, input.log_file, iter.first, run_func, t.size, res, t.sum);
        }
    }

    auto file = file_prep(input.filename.c_str());
    for (auto &i : vec)
    {
        write_result(i, file);
    }
    fclose(file);
}

void run_mul(const input_t<mul_func_t> &input, const test_vec_t &test_input, size_t max_size)
{
    if (input.vec.size() == 0)
    {
        return;
    }

    res_vec_t vec;
    double_mtx_vec_t res(max_size * max_size);

    for (auto &t : test_input)
    {
        if (t.size > max_size)
        {
            break;
        }

        auto l_data = t.mtx1;
        auto r_data = t.mtx2;
        auto dest_data = res.data();

        for (auto &iter : input.vec)
        {
            auto run_func = [l_data, r_data, dest_data, iter](size_t i)
            {
                iter.second(l_data, i, i, r_data, i, i, dest_data);
            };
            get_result(vec, input.log_file, iter.first, run_func, t.size, res, t.mul);
        }
    }

    auto file = file_prep(input.filename.c_str());
    for (auto &i : vec)
    {
        write_result(i, file);
    }
    fclose(file);
}

void run_lu(const input_t<lu_func_t> &input, const test_vec_t &test_input, size_t max_size)
{
    if (input.vec.size() == 0)
    {
        return;
    }

    res_vec_t vec;
    double_mtx_vec_t l(max_size * max_size);
    double_mtx_vec_t u(max_size * max_size);

    for (auto &t : test_input)
    {
        if (t.size > max_size)
        {
            break;
        }

        auto src_data = t.mtx1;
        auto l_data = l.data();
        auto u_data = u.data();

        for (auto &iter : input.vec)
        {
            auto run_func = [src_data, l_data, u_data, iter](size_t i)
            {
                iter.second(src_data, l_data, u_data, i);
            };
            get_result(vec, input.log_file, iter.first, run_func, t.size, l, u, t.mtx1);
        }
    }

    auto file = file_prep(input.filename.c_str());
    for (auto &i : vec)
    {
        write_result(i, file);
    }
    fclose(file);
}

void run_lup(const input_t<lup_func_t> &input, const test_vec_t &test_input, size_t max_size)
{
    if (input.vec.size() == 0)
    {
        return;
    }

    res_vec_t vec;
    double_mtx_vec_t l(max_size * max_size);
    double_mtx_vec_t u(max_size * max_size);
    std::vector<size_t> p(max_size);

    for (auto &t : test_input)
    {
        if (t.size > max_size)
        {
            break;
        }

        auto src_data = t.mtx1;
        auto l_data = l.data();
        auto u_data = u.data();
        auto p_data = p.data();

        for (auto &iter : input.vec)
        {
            auto run_func = [src_data, l_data, u_data, p_data, iter](size_t i)
            {
                iter.second(src_data, l_data, u_data, p_data, i);
            };
            get_result(vec, input.log_file, iter.first, run_func, t.size, l, u, p, t.mtx1);
        }
    }

    auto file = file_prep(input.filename.c_str());
    for (auto &i : vec)
    {
        write_result(i, file);
    }
    fclose(file);
}

void run_inverse(const input_t<inverse_func_t> &input, const test_vec_t &test_input, size_t max_size)
{
    if (input.vec.size() == 0)
    {
        return;
    }

    res_vec_t vec;
    double_mtx_vec_t res(max_size * max_size);

    for (auto &t : test_input)
    {
        if (t.size > max_size)
        {
            break;
        }

        auto src_data = t.mtx1;
        auto dest_data = res.data();

        for (auto &iter : input.vec)
        {
            auto run_func = [src_data, dest_data, iter](size_t i)
            {
                iter.second(src_data, dest_data, i);
            };
            get_result(vec, input.log_file, iter.first, run_func, t.size, res, t.inv);
        }
    }

    auto file = file_prep(input.filename.c_str());
    for (auto &i : vec)
    {
        write_result(i, file);
    }
    fclose(file);
}

void run_determinant(const input_t<determinant_func_t> &input, const test_vec_t &test_input, size_t max_size)
{
    if (input.vec.size() == 0)
    {
        return;
    }

    res_vec_t vec;
    double res;

    for (auto &t : test_input)
    {
        if (t.size > max_size)
        {
            break;
        }

        auto src_data = t.mtx1;
        auto dest_data = &res;

        for (auto &iter : input.vec)
        {
            auto run_func = [src_data, dest_data, iter](size_t i)
            {
                iter.second(src_data, dest_data, i);
            };
            get_result(vec, input.log_file, iter.first, run_func, t.size, res, t.det);
        }
    }

    auto file = file_prep(input.filename.c_str());
    for (auto &i : vec)
    {
        write_result(i, file);
    }
    fclose(file);
}

#ifdef ST

void finished(const char *name)
{
    fprintf(stdout, "%s finished\n", name);
    fflush(stdout);
}

#else

static std::mutex stdout_mtx;

void finished(const char *name)
{
    std::unique_lock l(stdout_mtx);
    fprintf(stdout, "%s finished\n", name);
    fflush(stdout);
}

#endif // ST

template <class T>
void worker_proc(input_t<T> &input, const test_vec_t &test_vec, size_t N)
{
    fprintf(input.log_file, "Running %s...\n", input.filename.c_str());
    input.experiment(input, test_vec, N);
    finished(input.filename.c_str());
}

int main()
{
    srand(6);
#ifdef NDEBUG
    const size_t N = 1ul << 10;
#else
    const size_t N = 100;
#endif

    test_vec_t test_vec;
    mem_t mem;
    read_mtx("test.bin", test_vec, mem);

    input_t<sum_func_t> sum_input;
    sum_input.filename = "sum.csv";
    sum_input.vec.emplace_back("avx512", matrix_avx512_double_sum);
    sum_input.vec.emplace_back("seq", matrix_seq_double_sum);
    sum_input.vec.emplace_back("avx2", matrix_avx2_double_sum);
    sum_input.experiment = run_sum;

    input_t<mul_func_t> mul_input;
    mul_input.filename = "mul.csv";
    mul_input.vec.emplace_back("avx512", matrix_avx512_double_mul);
    mul_input.vec.emplace_back("avx512_unlocal", matrix_avx512_double_mul_unlocal);
    mul_input.vec.emplace_back("seq", matrix_seq_double_mul);
    mul_input.vec.emplace_back("seq_local", matrix_seq_double_mul_local);
    mul_input.vec.emplace_back("seq_local2", matrix_seq_double_mul_local2);
    mul_input.vec.emplace_back("avx2", matrix_avx2_double_mul);
    mul_input.vec.emplace_back("avx2_unlocal", matrix_avx2_double_mul_unlocal);
    mul_input.experiment = run_mul;

    input_t<lu_func_t> lu_input;
    lu_input.filename = "lu.csv";
    lu_input.vec.emplace_back("avx512", matrix_avx512_double_lu);
    lu_input.vec.emplace_back("avx2", matrix_avx2_double_lu);
    lu_input.vec.emplace_back("seq", matrix_seq_double_lu);
    lu_input.experiment = run_lu;

    input_t<lup_func_t> lup_input;
    lup_input.filename = "lup.csv";
    lup_input.vec.emplace_back("avx512", matrix_avx512_double_lup);
    lup_input.vec.emplace_back("avx2", matrix_avx2_double_lup);
    lup_input.vec.emplace_back("seq", matrix_seq_double_lup);
    lup_input.experiment = run_lup;

    input_t<inverse_func_t> inverse_input;
    inverse_input.filename = "inverse.csv";
    inverse_input.vec.emplace_back("avx512", matrix_avx512_double_inverse);
    inverse_input.vec.emplace_back("avx2", matrix_avx2_double_inverse);
    inverse_input.vec.emplace_back("seq", matrix_seq_double_inverse);
    inverse_input.experiment = run_inverse;

    input_t<determinant_func_t> determinant_input;
    determinant_input.filename = "determinant.csv";
    determinant_input.vec.emplace_back("avx512", matrix_avx512_double_determinant);
    determinant_input.vec.emplace_back("avx2", matrix_avx2_double_determinant);
    determinant_input.vec.emplace_back("seq", matrix_seq_double_determinant);
    determinant_input.experiment = run_determinant;

#ifndef LOGTOSTDERR
    sum_input.log_file = fopen("sum.log", "w");
    mul_input.log_file = fopen("mul.log", "w");
    lu_input.log_file = fopen("lu.log", "w");
    lup_input.log_file = fopen("lup.log", "w");
    inverse_input.log_file = fopen("inverse.log", "w");
    determinant_input.log_file = fopen("determinant.log", "w");
#endif

#ifdef ST
    worker_proc<sum_func_t>(sum_input, test_vec, N);
    worker_proc<mul_func_t>(mul_input, test_vec, N);
    worker_proc<lu_func_t>(lu_input, test_vec, N);
    worker_proc<lup_func_t>(lup_input, test_vec, N);
    worker_proc<inverse_func_t>(inverse_input, test_vec, N);
    worker_proc(determinant_input, test_vec, N);
#else
    std::vector<std::thread> workers;

    workers.emplace_back(worker_proc<sum_func_t>, std::ref(sum_input), std::ref(test_vec), N);
    workers.emplace_back(worker_proc<mul_func_t>, std::ref(mul_input), std::ref(test_vec), N);
    workers.emplace_back(worker_proc<lu_func_t>, std::ref(lu_input), std::ref(test_vec), N);
    workers.emplace_back(worker_proc<lup_func_t>, std::ref(lup_input), std::ref(test_vec), N);
    workers.emplace_back(worker_proc<inverse_func_t>, std::ref(inverse_input), std::ref(test_vec), N);
    worker_proc(determinant_input, test_vec, N);

    for (auto &i : workers)
    {
        i.join();
    }
#endif // ST
}
