#include <string.h>
#include <stdlib.h>
#include <math.h>

#include "seq.h"

#define SQUARE(x) ((x) * (x))
#define SQUARE_DOUBLE_MATRIX(x) (SQUARE(x) * sizeof(double))
#define DOUBLE_MATRIX(x, y) ((x) * (y) * sizeof(double))

#define GET_VAL(m, r, c, row_c) (((m) + (r) + (c) * (row_c)))
#define GET_COLUMN(c, x, y, col_c) (((c) + (x) + (y) * (col_c)))

int matrix_seq_double_sum(const_double_matrix_t left, const_double_matrix_t right, double_matrix_t dest, size_t r, size_t c)
{
    for (size_t i = 0; i < (r * c); ++i)
    {
        dest[i] = left[i] + right[i];
    }
    return 0;
}

int matrix_seq_double_gaussian(const_double_matrix_t src, double_matrix_t dest, size_t size)
{
    memcpy(dest, src, SQUARE_DOUBLE_MATRIX(size));

    for (size_t i = 0; i < size; ++i)
    {
        for (size_t j = i + 1; j < size; ++j)
        {
            double k = *GET_VAL(dest, i, j, size) / *GET_VAL(dest, i, i, size);
            for (size_t t = i; t < size; ++t)
            {
                *GET_VAL(dest, t, j, size) = *GET_VAL(dest, t, j, size) - k * (*GET_VAL(dest, t, i, size));
            }
        }
    }
    return 0;
}

int matrix_seq_double_lu(const_double_matrix_t src, double_matrix_t L, double_matrix_t U, size_t size)
{
    memcpy(L, src, SQUARE_DOUBLE_MATRIX(size));
    memset(U, 0, SQUARE_DOUBLE_MATRIX(size));

    for (size_t i = 0; i < size; ++i)
    {
        *GET_VAL(U, i, i, size) = 1.0;
        for (size_t j = i + 1; j < size; ++j)
        {
            double k = *GET_VAL(L, i, j, size) / *GET_VAL(L, i, i, size);
            *GET_VAL(U, i, j, size) = k;
            for (size_t t = i; t < size; ++t)
            {
                *GET_VAL(L, t, j, size) = *GET_VAL(L, t, j, size) - k * (*GET_VAL(L, t, i, size));
            }
        }
    }
    return 0;
}

int matrix_seq_double_lup(const_double_matrix_t src, double_matrix_t L, double_matrix_t U, size_t *P, size_t size)
{
    memcpy(L, src, SQUARE_DOUBLE_MATRIX(size));
    memset(U, 0, SQUARE_DOUBLE_MATRIX(size));

    for (size_t i = 0; i < size; ++i)
    {
        P[i] = i;
    }

    double *column = malloc(sizeof(double) * size);
    for (size_t i = 0; i < size; ++i)
    {
        if (!L[i * size + i])
        {
            size_t j;
            for (j = i + 1; j < size; ++j)
                if (!!L[j * size + i])
                {
                    size_t zz = P[j];
                    P[j] = P[i];
                    P[i] = zz;

                    memcpy(column, GET_VAL(L, 0, i, size), size * sizeof(double));
                    memcpy(GET_VAL(L, 0, i, size), GET_VAL(L, 0, j, size), size * sizeof(double));
                    memcpy(GET_VAL(L, 0, j, size), column, size * sizeof(double));

                    memcpy(column, GET_VAL(U, 0, i, size), size * sizeof(double));
                    memcpy(GET_VAL(U, 0, i, size), GET_VAL(U, 0, j, size), size * sizeof(double));
                    memcpy(GET_VAL(U, 0, j, size), column, size * sizeof(double));
                    break;
                }
            if (j == size)
                continue;
        }

        for (size_t j = i + 1; j < size; ++j)
        {
            double k = *GET_VAL(L, i, j, size) / *GET_VAL(L, i, i, size);
            *GET_VAL(U, i, j, size) = k;
            for (size_t t = i; t < size; ++t)
            {
                *GET_VAL(L, t, j, size) = *GET_VAL(L, t, j, size) - k * (*GET_VAL(L, t, i, size));
            }
        }
    }
    for (size_t i = 0; i < size; ++i)
    {
        *GET_VAL(U, i, i, size) = 1.0;
    }

    free(column);
    return 0;
}

int matrix_seq_double_inverse(const_double_matrix_t src, double_matrix_t dest, size_t size)
{
    double_matrix_t L = malloc(SQUARE_DOUBLE_MATRIX(size));

    memset(dest, 0, SQUARE_DOUBLE_MATRIX(size));
    memcpy(L, src, SQUARE_DOUBLE_MATRIX(size));

    for (size_t i = 0; i < size; ++i)
    {
        *GET_VAL(dest, i, i, size) = 1.0;
    }

    double *column = malloc(sizeof(double) * size);
    for (size_t i = 0; i < size; ++i)
    {
        size_t maxi = i;
        double max = fabs(*GET_VAL(L, i, i, size));
        for (size_t j = i + 1; j < size; ++j)
        {
            double k = fabs(*GET_VAL(L, i, j, size));
            if (k > max)
            {
                max = k;
                maxi = j;
            }
        }

        if (maxi != i)
        {
            memcpy(column, GET_VAL(L, 0, i, size), size * sizeof(double));
            memcpy(GET_VAL(L, 0, i, size), GET_VAL(L, 0, maxi, size), size * sizeof(double));
            memcpy(GET_VAL(L, 0, maxi, size), column, size * sizeof(double));

            memcpy(column, GET_VAL(dest, 0, i, size), size * sizeof(double));
            memcpy(GET_VAL(dest, 0, i, size), GET_VAL(dest, 0, maxi, size), size * sizeof(double));
            memcpy(GET_VAL(dest, 0, maxi, size), column, size * sizeof(double));
        }

        double k = *GET_VAL(L, i, i, size);
        for (size_t j = 0; j < size; ++j)
        {
            *GET_VAL(L, j, i, size) = *GET_VAL(L, j, i, size) / k;
            *GET_VAL(dest, j, i, size) = *GET_VAL(dest, j, i, size) / k;
        }

        for (size_t j = 0; j < size; ++j)
        {
            if (j != i)
            {
                k = *GET_VAL(L, i, j, size);
                for (size_t t = 0; t < size; ++t)
                {
                    *GET_VAL(L, t, j, size) = *GET_VAL(L, t, j, size) - k * (*GET_VAL(L, t, i, size));
                    *GET_VAL(dest, t, j, size) = *GET_VAL(dest, t, j, size) - k * *GET_VAL(dest, t, i, size);
                }
            }
        }
    }

    free(column);
    free(L);
    return 0;
}

int matrix_seq_double_determinant(const_double_matrix_t src, double *res, size_t size)
{
    double_matrix_t L = malloc(SQUARE_DOUBLE_MATRIX(size));

    memcpy(L, src, SQUARE_DOUBLE_MATRIX(size));
    *res = 1.0;

    double *column = malloc(sizeof(double) * size);
    for (size_t i = 0; i < size; ++i)
    {
        size_t maxi = i;
        double max = fabs(*GET_VAL(L, i, i, size));
        for (size_t j = i + 1; j < size; ++j)
        {
            double k = fabs(*GET_VAL(L, i, j, size));
            if (k > max)
            {
                max = k;
                maxi = j;
            }
        }

        if (maxi != i)
        {
            *res *= -1.0;

            memcpy(column, GET_VAL(L, 0, i, size), size * sizeof(double));
            memcpy(GET_VAL(L, 0, i, size), GET_VAL(L, 0, maxi, size), size * sizeof(double));
            memcpy(GET_VAL(L, 0, maxi, size), column, size * sizeof(double));
        }

        for (size_t j = i + 1; j < size; ++j)
        {
            double k = *GET_VAL(L, i, j, size) / *GET_VAL(L, i, i, size);
            for (size_t t = i; t < size; ++t)
            {
                *GET_VAL(L, t, j, size) = *GET_VAL(L, t, j, size) - k * (*GET_VAL(L, t, i, size));
            }
        }
        *res *= *GET_VAL(L, i, i, size);
    }

    free(column);
    free(L);
    return 0;
}

int matrix_seq_double_apply(const_double_matrix_t src, size_t *P, double_matrix_t dest, size_t size)
{
    for (size_t i = 0; i < size; ++i)
    {
        memcpy(GET_VAL(dest, 0, i, size), GET_VAL(src, 0, P[i], size), size * sizeof(double));
    }
    return 0;
}

int matrix_seq_double_mul(const_double_matrix_t left, size_t r1, size_t c1,
                          const_double_matrix_t right, size_t r2, size_t c2,
                          double_matrix_t dest)
{
    if (c1 != r2)
    {
        return -1;
    }

    for (size_t i = 0; i < c2; ++i)
    {
        for (size_t j = 0; j < r1; ++j)
        {
            dest[i * r1 + j] = 0;
            for (size_t k = 0; k < r2; ++k)
                dest[i * r1 + j] += left[k * r1 + j] * right[i * r2 + k];
        }
    }

    return 0;
}

int matrix_seq_double_mul_local(const_double_matrix_t left, size_t r1, size_t c1,
                                const_double_matrix_t right, size_t r2, size_t c2,
                                double_matrix_t dest)
{
    if (c1 != r2)
    {
        return -1;
    }

    for (size_t col = 0; col < c2; ++col)
    {
        memset(dest + col*r1, 0, sizeof(double)*r1);
        for (size_t l_col = 0; l_col < c1; ++l_col)
        {
            double k = right[col * r2 + l_col];
            for (size_t row = 0; row < r1; ++row)
            {
                dest[col*r1 + row] += left[l_col*r1 + row] * k;
            }
        }
    }

    return 0;
}

int matrix_seq_double_mul_local2(const_double_matrix_t left, size_t r1, size_t c1,
                                 const_double_matrix_t right, size_t r2, size_t c2,
                                 double_matrix_t dest)
{
    const size_t doubles_in_vec = 8;

    if (c1 != r2)
    {
        return -1;
    }
    size_t n = r1 / doubles_in_vec;
    double *columns = malloc(n * c1 * sizeof(double)*doubles_in_vec);
    for (size_t i = 0; i < n; ++i)
    {
        for (size_t j = 0; j < c1; ++j)
        {
            memcpy(columns + (i*c1 + j) * doubles_in_vec, left + j*r1 + i * doubles_in_vec, sizeof(double) * doubles_in_vec);
        }
    }

    for (size_t c = 0; c < n; ++c)
    {
        for (size_t i = 0; i < c2; ++i)
        {
            memset(dest + c*doubles_in_vec + i * r1, 0, sizeof(double) * doubles_in_vec);
            for (size_t j = 0; j < c1; ++j)
            {
                double k_ = right[i * r2 + j];
                for (size_t k = 0; k < doubles_in_vec; ++k)
                {
                    dest[c*doubles_in_vec + k + i * r1] += columns[k + (j + c * c1)*doubles_in_vec] * k_;
                }
            }
        }
    }
    free(columns);
    return 0;
}
