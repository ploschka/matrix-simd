#include <immintrin.h>
#include <stdlib.h>
#include <math.h>
#include <string.h>
#include <stdio.h>
#include <assert.h>

#include "avx512.h"

static const size_t doubles_in_vec = 8;

#define SQUARE(x) ((x) * (x))
#define SQUARE_DOUBLE_MATRIX(x) (SQUARE(x) * sizeof(double))
#define DOUBLE_MATRIX(x, y) ((x) * (y) * sizeof(double))

#define GET_VAL(m, r, c, row_c) (((m) + (r) + (c) * (row_c)))
#define GET_COLUMN(c, x, y, col_c) (((c) + (x) + (y) * (col_c)))
#define STR2(X) #X
#define STR(X) STR2(X)
#define verify(x)                                                                                 \
    do                                                                                            \
    {                                                                                             \
        if (!(x))                                                                                 \
        {                                                                                         \
            fprintf(stderr, "Constraint violation at %s (%u): %s\n", __FILE__, __LINE__, STR(x)); \
            exit(__LINE__);                                                                       \
        }                                                                                         \
    } while (0)
#define is_avx512_aligned(n) ((n) % (sizeof(__m512d) / sizeof(double)) == 0)

int matrix_avx512_double_sum(const_double_matrix_t left, const_double_matrix_t right, double_matrix_t dest, size_t r, size_t c)
{
    size_t t = (r * c);
    for (size_t i = 0; i < t; i += doubles_in_vec)
    {
        __m512d _left = _mm512_loadu_pd(left + i);
        __m512d _right = _mm512_loadu_pd(right + i);
        _mm512_storeu_pd(dest + i, _mm512_add_pd(_left, _right));
    }
    return 0;
}

int matrix_avx512_double_gaussian(const_double_matrix_t src, double_matrix_t dest, size_t size)
{
    memcpy(dest, src, size * size * sizeof src[0]);

    for (size_t i = 0; i < size; ++i)
    {
        for (size_t j = i + 1; j < size; ++j)
        {
            double k = *GET_VAL(dest, i, j, size) / *GET_VAL(dest, i, i, size);
            size_t unaligned_rows = i % doubles_in_vec;
            __m512d kv = _mm512_set1_pd(k);
            for (size_t row = i, next_row = i + doubles_in_vec; next_row <= size; row = next_row, next_row += doubles_in_vec)
            {
                __m512d val = _mm512_fnmadd_pd(kv, _mm512_loadu_pd(GET_COLUMN(dest, row, i, size)),
                                               _mm512_loadu_pd(GET_COLUMN(dest, row, j, size)));
                _mm512_storeu_pd(GET_VAL(dest, row, j, size), val);
            }
            if (!!unaligned_rows)
            {
                size_t row = size - doubles_in_vec;
                __mmask8 done_rows = ~0ul << unaligned_rows;
                __m512d val = _mm512_mask3_fnmadd_pd(kv, _mm512_loadu_pd(GET_COLUMN(dest, row, i, size)), _mm512_loadu_pd(GET_COLUMN(dest, row, j, size)), done_rows);
                _mm512_storeu_pd(GET_VAL(dest, row, j, size), val);
            }
        }
    }
    return 0;
}

int matrix_avx512_double_lu(const_double_matrix_t src, double_matrix_t L, double_matrix_t U, size_t size)
{
    memcpy(L, src, size * size * sizeof(double));
    memset(U, 0, SQUARE_DOUBLE_MATRIX(size));

    for (size_t i = 0; i < size; ++i)
    {
        for (size_t j = i + 1; j < size; ++j)
        {
            double k = *GET_VAL(L, i, j, size) / *GET_VAL(L, i, i, size);
            size_t unaligned_rows = i % doubles_in_vec;
            __m512d kv = _mm512_set1_pd(k);
            *GET_VAL(U, i, j, size) = k;

            for (size_t row = i, next_row = i + doubles_in_vec; next_row <= size; row = next_row, next_row += doubles_in_vec)
            {
                __m512d val = _mm512_fnmadd_pd(kv, _mm512_loadu_pd(GET_COLUMN(L, row, i, size)), _mm512_loadu_pd(GET_COLUMN(L, row, j, size)));
                _mm512_storeu_pd(GET_VAL(L, row, j, size), val);
            }
            if (!!unaligned_rows)
            {
                size_t row = size - doubles_in_vec;
                __mmask8 done_rows = ~0ul << unaligned_rows;
                __m512d val = _mm512_mask3_fnmadd_pd(kv, _mm512_loadu_pd(GET_COLUMN(L, row, i, size)), _mm512_loadu_pd(GET_COLUMN(L, row, j, size)), done_rows);
                _mm512_storeu_pd(GET_VAL(L, row, j, size), val);
            }
        }
    }
    for (size_t i = 0; i < size; ++i)
    {
        *GET_VAL(U, i, i, size) = 1.0;
    }
    return 0;
}

static void swap_mem(double *l, double *r, size_t n)
{
    assert(is_avx512_aligned(n));
    for (size_t i = 0; i < n; i += doubles_in_vec)
    {
        __m512d lv = _mm512_loadu_pd(&l[i]), rv = _mm512_loadu_pd(&r[i]);
        lv = _mm512_xor_pd(lv, rv);
        rv = _mm512_xor_pd(lv, rv);
        lv = _mm512_xor_pd(lv, rv);
        _mm512_storeu_pd(&l[i], lv);
        _mm512_storeu_pd(&r[i], rv);
    }
}

int matrix_avx512_double_lup_(const_double_matrix_t src, double_matrix_t L, double_matrix_t U, size_t *P, size_t size)
{
    size_t n = size / doubles_in_vec;
    __m512d *columns = _mm_malloc(n * size * sizeof(__m512d), 64);
    memset(U, 0, SQUARE_DOUBLE_MATRIX(size));
    for (size_t i = 0; i < size; ++i)
    {
        P[i] = i;
        *GET_VAL(L, 0, i, size) = *GET_VAL(src, 0, i, size);
    }
    for (size_t i = 0; i < n; ++i)
    {
        for (size_t j = 0; j < size; ++j)
        {
            *GET_COLUMN(columns, j, i, size) = _mm512_loadu_pd(GET_VAL(src, i * doubles_in_vec, j, size));
        }
    }

    double *column = malloc(sizeof(double) * size);
    for (size_t i = 0; i < size; ++i)
    {
        size_t maxi = i;
        double max = fabs(*GET_VAL(L, i, i, size));
        for (size_t j = i + 1; j < size; ++j)
        {
            double k = fabs(*GET_VAL(L, i, j, size));
            if (k > max)
            {
                max = k;
                maxi = j;
            }
        }

        if (maxi != i)
        {
            size_t zz = P[maxi];
            P[maxi] = P[i];
            P[i] = zz;

            memcpy(column, GET_VAL(L, 0, i, size), size * sizeof(double));
            memcpy(GET_VAL(L, 0, i, size), GET_VAL(L, 0, maxi, size), size * sizeof(double));
            memcpy(GET_VAL(L, 0, maxi, size), column, size * sizeof(double));

            memcpy(column, GET_VAL(U, 0, i, size), size * sizeof(double));
            memcpy(GET_VAL(U, 0, i, size), GET_VAL(U, 0, maxi, size), size * sizeof(double));
            memcpy(GET_VAL(U, 0, maxi, size), column, size * sizeof(double));

            for (size_t col = 0; col < n; ++col)
            {
                __m512d z = *GET_COLUMN(columns, maxi, col, size);
                *GET_COLUMN(columns, maxi, col, size) = *GET_COLUMN(columns, i, col, size);
                *GET_COLUMN(columns, i, col, size) = z;
            }
        }

        for (size_t j = i + 1; j < size; ++j)
        {
            double k = *GET_VAL(L, i, j, size) / *GET_VAL(L, i, i, size);
            __m512d kv = _mm512_set1_pd(k);
            *GET_VAL(U, i, j, size) = k;

            for (size_t col = 0; col < n; ++col)
            {
                __m512d m = _mm512_mul_pd(kv, *GET_COLUMN(columns, i, col, size));
                *GET_COLUMN(columns, j, col, size) = _mm512_sub_pd(*GET_COLUMN(columns, j, col, size), m);
                _mm512_storeu_pd(GET_VAL(L, doubles_in_vec * col, j, size), *GET_COLUMN(columns, j, col, size));
            }
        }
        for (size_t col = 0; col < n; ++col)
        {
            _mm512_storeu_pd(GET_VAL(L, doubles_in_vec * col, i, size), *GET_COLUMN(columns, i, col, size));
        }
    }
    for (size_t i = 0; i < size; ++i)
    {
        *GET_VAL(U, i, i, size) = 1.0;
    }
    free(column);
    _mm_free(columns);
    return 0;
}

int matrix_avx512_double_lup(const_double_matrix_t src, double_matrix_t L, double_matrix_t U, size_t *P, size_t size)
{
    memcpy(L, src, size * size * sizeof(double));
    memset(U, 0, SQUARE_DOUBLE_MATRIX(size));
    for (size_t i = 0; i < size; ++i)
        P[i] = i;

    for (size_t i = 0; i < size; ++i)
    {
        if (!L[i * size + i])
        {
            size_t j;
            for (j = i + 1; j < size; ++j)
                if (!!L[j * size + i])
                {
                    size_t p = P[i];
                    P[i] = P[j];
                    P[j] = p;
                    swap_mem(L + i * size, L + j * size, size);
                    swap_mem(U + i * size, U + j * size, size);
                    break;
                }
            if (j == size)
                continue;
        }

        for (size_t j = i + 1; j < size; ++j)
        {
            double k = *GET_VAL(L, i, j, size) / *GET_VAL(L, i, i, size);
            size_t unaligned_rows = i % doubles_in_vec;
            __m512d kv = _mm512_set1_pd(k);
            *GET_VAL(U, i, j, size) = k;

            for (size_t row = i, next_row = i + doubles_in_vec; next_row <= size; row = next_row, next_row += doubles_in_vec)
            {
                __m512d val = _mm512_fnmadd_pd(kv, _mm512_loadu_pd(GET_COLUMN(L, row, i, size)), _mm512_loadu_pd(GET_COLUMN(L, row, j, size)));
                _mm512_storeu_pd(GET_VAL(L, row, j, size), val);
            }
            if (!!unaligned_rows)
            {
                size_t row = size - doubles_in_vec;
                __mmask8 done_rows = ~0ul << unaligned_rows;
                __m512d val = _mm512_mask3_fnmadd_pd(kv, _mm512_loadu_pd(GET_COLUMN(L, row, i, size)), _mm512_loadu_pd(GET_COLUMN(L, row, j, size)), done_rows);
                _mm512_storeu_pd(GET_VAL(L, row, j, size), val);
            }
        }
    }
    for (size_t i = 0; i < size; ++i)
    {
        *GET_VAL(U, i, i, size) = 1.0;
    }
    return 0;
}

int matrix_avx512_double_inverse(const_double_matrix_t src, double_matrix_t dest, size_t size)
{
    double_matrix_t L = malloc(SQUARE_DOUBLE_MATRIX(size));
    memcpy(L, src, SQUARE_DOUBLE_MATRIX(size));
    memset(dest, 0, SQUARE_DOUBLE_MATRIX(size));
    for (size_t i = 0; i < size; ++i)
    {
        *GET_VAL(dest, i, i, size) = 1.0;
    }

    for (size_t i = 0; i < size; ++i)
    {
        if (!L[i * size + i])
        {
            size_t j;
            for (j = i + 1; j < size; ++j)
                if (!!L[j * size + i])
                {
                    swap_mem(L + i * size, L + j * size, size);
                    swap_mem(dest + i * size, dest + j * size, size);
                    break;
                }
            if (j == size)
                continue;
        }

        double k = *GET_VAL(L, i, i, size);
        __m512d kv = _mm512_set1_pd(k);

        for (size_t row = 0; row < size; row += doubles_in_vec)
        {
            __m512d val = _mm512_div_pd(_mm512_loadu_pd(GET_COLUMN(L, row, i, size)), kv);
            _mm512_storeu_pd(GET_VAL(L, row, i, size), val);

            val = _mm512_div_pd(_mm512_loadu_pd(GET_COLUMN(dest, row, i, size)), kv);
            _mm512_storeu_pd(GET_VAL(dest, row, i, size), val);
        }

        for (size_t j = 0; j < size; ++j)
        {
            if (i != j)
            {
                k = *GET_VAL(L, i, j, size);
                kv = _mm512_set1_pd(k);

                for (size_t row = 0; row < size; row += doubles_in_vec)
                {
                    __m512d val = _mm512_fnmadd_pd(kv, _mm512_loadu_pd(GET_COLUMN(L, row, i, size)), _mm512_loadu_pd(GET_COLUMN(L, row, j, size)));
                    _mm512_storeu_pd(GET_VAL(L, row, j, size), val);

                    val = _mm512_fnmadd_pd(kv, _mm512_loadu_pd(GET_COLUMN(dest, row, i, size)), _mm512_loadu_pd(GET_COLUMN(dest, row, j, size)));
                    _mm512_storeu_pd(GET_VAL(dest, row, j, size), val);
                }
            }
        }
    }
    free(L);
    return 0;
}

void matrix_avx512_permute_columns(double *m, const size_t *p, size_t n)
{
    for (size_t c = 0; c < n; ++c)
    {
        size_t t, tt, h;
        if (c == p[c])
            continue;
        t = h = c;
        do
        {
            t = p[t];
            h = p[p[h]];
        } while (h >= c && t >= c && h != t);
        if (h != t)
            continue;
        while ((tt = p[t]) != h)
        {
            swap_mem(&m[t * n], &m[tt * n], n);
            t = tt;
        }
    }
}

int matrix_avx512_double_determinant(const_double_matrix_t src, double *res, size_t size)
{
    double_matrix_t L = malloc(SQUARE_DOUBLE_MATRIX(size));
    memcpy(L, src, size * size * sizeof(double));
    *res = 1.0;

    for (size_t i = 0; i < size; ++i)
    {
        if (!L[i * size + i])
        {
            size_t j;
            for (j = i + 1; j < size; ++j)
                if (!!L[j * size + i])
                {
                    *res *= 1.0;

                    swap_mem(L + i * size, L + j * size, size);
                    break;
                }
            if (j == size)
                continue;
        }

        for (size_t j = i + 1; j < size; ++j)
        {
            double k = *GET_VAL(L, i, j, size) / *GET_VAL(L, i, i, size);
            size_t unaligned_rows = i % doubles_in_vec;
            __m512d kv = _mm512_set1_pd(k);

            for (size_t row = i, next_row = i + doubles_in_vec; next_row <= size; row = next_row, next_row += doubles_in_vec)
            {
                __m512d val = _mm512_fnmadd_pd(kv, _mm512_loadu_pd(GET_COLUMN(L, row, i, size)), _mm512_loadu_pd(GET_COLUMN(L, row, j, size)));
                _mm512_storeu_pd(GET_VAL(L, row, j, size), val);
            }
            if (!!unaligned_rows)
            {
                size_t row = size - doubles_in_vec;
                __mmask8 done_rows = ~0ul << unaligned_rows;
                __m512d val = _mm512_mask3_fnmadd_pd(kv, _mm512_loadu_pd(GET_COLUMN(L, row, i, size)), _mm512_loadu_pd(GET_COLUMN(L, row, j, size)), done_rows);
                _mm512_storeu_pd(GET_VAL(L, row, j, size), val);
            }
        }
        *res *= *GET_VAL(L, i, i, size);
    }
    free(L);
    return 0;
}

int matrix_avx512_double_apply(const_double_matrix_t src, const size_t *P, double_matrix_t dest, size_t size)
{
    for (size_t i = 0; i < size; ++i)
    {
        memcpy(GET_VAL(dest, 0, i, size), GET_VAL(src, 0, P[i], size), size * sizeof(double));
    }
    return 0;
}

int matrix_avx512_double_mul(const_double_matrix_t left, size_t r1, size_t c1,
                             const_double_matrix_t right, size_t r2, size_t c2,
                             double_matrix_t dest)
{
    if (c1 != r2)
    {
        return -1;
    }
    size_t n = r1 / doubles_in_vec;
    __m512d *columns = _mm_malloc(n * c1 * sizeof(__m512d), 64);
    __m512d curr;
    for (size_t i = 0; i < n; ++i)
    {
        for (size_t j = 0; j < c1; ++j)
        {
            *GET_COLUMN(columns, j, i, c1) = _mm512_loadu_pd(GET_VAL(left, i * doubles_in_vec, j, r1));
        }
    }

    for (size_t c = 0; c < n; ++c)
    {
        for (size_t i = 0; i < c2; ++i)
        {
            curr = _mm512_setzero_pd();
            for (size_t j = 0; j < c1; ++j)
            {
                curr = _mm512_fmadd_pd(_mm512_set1_pd(*GET_VAL(right, j, i, r2)), *GET_COLUMN(columns, j, c, c1), curr);
            }
            _mm512_storeu_pd(GET_VAL(dest, c * doubles_in_vec, i, r1), curr);
        }
    }
    _mm_free(columns);
    return 0;
}

int matrix_avx512_double_mul_unlocal(const_double_matrix_t left, size_t r1, size_t c1,
                                     const_double_matrix_t right, size_t r2, size_t c2,
                                     double_matrix_t dest)
{
    verify(c1 == r2);
    size_t n = r1 / doubles_in_vec;

    for (size_t c = 0; c < n; ++c)
    {
        for (size_t i = 0; i < c2; ++i)
        {
            __m512d curr = _mm512_setzero_pd();
            for (size_t j = 0; j < c1; ++j)
                curr = _mm512_fmadd_pd(_mm512_set1_pd(*GET_VAL(right, j, i, r2)), _mm512_loadu_pd(GET_COLUMN(left, c * doubles_in_vec, j, c1)), curr);
            _mm512_storeu_pd(GET_VAL(dest, c * doubles_in_vec, i, r1), curr);
        }
    }
    return 0;
}
