AVX512 := -mavx512f -mavx512cd -mavx512dq -mavx512bw -mavx512vl -mfma
ifeq ($(DEBUG_BUILD),)
DEBREL := -O3 -g -DNDEBUG
else
DEBREL := -O0 -g
endif
ifneq ($(LOGTOSTDERR),)
DEBREL := $(DEBREL) -DLOGTOSTDERR
endif
ifneq ($(SINGLETHREAD),)
DEBREL := $(DEBREL) -DST
endif

CC=gcc
CXX=g++
CFLAGS=-Wall -Wextra -pedantic
CXXFLAGS=--std=c++20

objects=avx512.o avx2.o seq.o global.o matrix.o sum-avx.o sum.o

.PHONY: all clean test fresh

all: run e.out

clean:
	rm -f run *.obj *.o *.out *.temp *.log *.asm

fresh: clean all

run: avx512.o avx2.o entrypoint.obj global.o
	$(CXX) $(DEBREL) $^ -o $@ -pthread

sum.obj:sum.c
	$(CC) $(AVX512) $(DEBREL) -c sum.c -o sum.obj
matrix.obj:matrix.c
	$(CC) $(AVX512) $(DEBREL) -c matrix.c -o matrix.obj
sum-avx.obj:sum-avx.c
	$(CC) $(DEBREL) -mavx -c $< -o $@

entrypoint.obj:entrypoint.cpp
	$(CXX) $(DEBREL) --std=c++20 -c $< -o $@ -pthread

test:run
	./run && echo Success || echo Failure

$(objects): %.o: %.c
	$(CC) $(DEBREL) $(CFLAGS) $(AVX512) $< -c -o $@

e.o: e.cpp
	$(CXX) $(CXXFLAGS) $(DEBREL) $< -c -o $@

e.out: entry.cpp e.o $(objects)
	$(CXX) $(DEBREL) $(CXXFLAGS) $(AVX512) $^ -o $@ -pthread

