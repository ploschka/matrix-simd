#pragma once
#include <vector>
#include <cstdio>
#include <functional>
#include <string>

#include "global.h"

struct test_t
{
    size_t size;

    const_double_matrix_t mtx1;
    const_double_matrix_t mtx2;

    const_double_matrix_t sum;
    const_double_matrix_t mul;
    const_double_matrix_t inv;
    double det;
};

typedef std::vector<test_t> test_vec_t;

template <class T>
struct input_t
{
    std::string filename;
    std::function<void(const input_t<T> &input, const test_vec_t &test_input, size_t max_size)> experiment;
    std::vector<std::pair<std::string, T>> vec;

    FILE *log_file = stderr;
    ~input_t()
    {
        if (log_file != stderr)
            fclose(log_file);
    }
};

struct result_t
{
    std::string name;
    double time;
    double ulp;
    size_t size;
};

struct mem_t
{
    size_t len;
    void *addr;
};

typedef int (*sum_func_t)(const_double_matrix_t left, const_double_matrix_t right, double_matrix_t dest, size_t r, size_t c);
typedef int (*mul_func_t)(const_double_matrix_t left, size_t r1, size_t c1, const_double_matrix_t right, size_t r2, size_t c2, double_matrix_t dest);
typedef int (*gaussian_func_t)(const_double_matrix_t src, double_matrix_t dest, size_t size);
typedef int (*lu_func_t)(const_double_matrix_t src, double_matrix_t L, double_matrix_t U, size_t size);
typedef int (*lup_func_t)(const_double_matrix_t src, double_matrix_t L, double_matrix_t U, size_t *P, size_t size);
typedef int (*inverse_func_t)(const_double_matrix_t src, double_matrix_t dest, size_t size);
typedef int (*determinant_func_t)(const_double_matrix_t src, double *res, size_t size);

typedef std::vector<result_t> res_vec_t;
typedef std::vector<double> double_mtx_vec_t;

static const size_t size_rate = 8;
static const char *pattern = "%s,%.3f,%.0f,%lu\n";
static const size_t repeat_times = 3;

void dbg_print(auto &&...x);

FILE *file_prep(const char *name);

void rand_init(double_mtx_vec_t &vec1, double_mtx_vec_t &vec2);
void rand_init(double_mtx_vec_t &vec);

void get_result(res_vec_t &vec, FILE *log_out, const std::string &name,
                std::function<void(size_t)> func, size_t i,
                const double_mtx_vec_t &res, const_double_matrix_t example);

void get_result(res_vec_t &vec, FILE *log_out, const std::string &name,
                std::function<void(size_t)> func, size_t i,
                double &res, double example);

void get_result(res_vec_t &vec, FILE *log_out, const std::string &name,
                std::function<void(size_t)> func, size_t size,
                const double_mtx_vec_t &res1, const double_mtx_vec_t &res2,
                const_double_matrix_t example);

void get_result(res_vec_t &vec, FILE *log_out, const std::string &name,
                std::function<void(size_t)> func, size_t size,
                const double_mtx_vec_t &res1, const double_mtx_vec_t &res2,
                const std::vector<size_t> resp, const_double_matrix_t example);

void write_result(const result_t &res, FILE *file);

void read_mtx(const char *filename, test_vec_t &vec, mem_t &mem);
void close_mtx(mem_t &mem);
