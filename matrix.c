#include <immintrin.h>
#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include <string.h>

const size_t doubles_in_vec = 8;

#define SQUARE(x) ((x) * (x))
#define SQUARE_DOUBLE_MATRIX(x) (SQUARE(x) * sizeof(double))
#define DOUBLE_MATRIX(x, y) ((x) * (y) * sizeof(double))

#define GET_VAL(m, r, c, row_c) (((m) + (r) + (c) * (row_c)))
#define GET_COLUMN(c, x, y, col_c) (((c) + (x) + (y) * (col_c)))

typedef double *double_matrix_t; //в заголовочный файл

void matrix_double_print(double_matrix_t m, size_t r, size_t c);

void matrix_double_gaussian(double_matrix_t src, double_matrix_t dest, size_t size)
{
    size_t n = size / doubles_in_vec;
    __m512d *columns = malloc(n * size * sizeof(__m512d));
    for (size_t i = 0; i < n; ++i)
    {
        for (size_t j = 0; j < size; ++j)
        {
            *GET_COLUMN(columns, j, i, size) = _mm512_loadu_pd(GET_VAL(src, i * doubles_in_vec, j, size));
        }
    }

    for (size_t i = 0; i < size; ++i)
    {
        *GET_VAL(dest, 0, i, size) = *GET_VAL(src, 0, i, size);
    }

    for (size_t i = 0; i < size; ++i)
    {
        for (size_t j = i + 1; j < size; ++j)
        {
            double k = *GET_VAL(dest, i, j, size) / *GET_VAL(dest, i, i, size);
            __m512d kv = _mm512_set1_pd(k);

            for (size_t col = 0; col < n; ++col)
            {
                *GET_COLUMN(columns, j, col, size) = _mm512_fnmadd_pd(kv, *GET_COLUMN(columns, i, col, size), *GET_COLUMN(columns, j, col, size));
                _mm512_storeu_pd(GET_VAL(dest, doubles_in_vec * col, j, size), *GET_COLUMN(columns, j, col, size));
            }
        }
        for (size_t col = 0; col < n; ++col)
        {
            _mm512_storeu_pd(GET_VAL(dest, doubles_in_vec * col, i, size), *GET_COLUMN(columns, i, col, size));
        }
    }
    free(columns);
}

void matrix_double_lu(double_matrix_t src, double_matrix_t L, double_matrix_t U, size_t size)
{
    size_t n = size / doubles_in_vec;
    __m512d *columns = malloc(n * size * sizeof(__m512d));
    for (size_t i = 0; i < n; ++i)
    {
        for (size_t j = 0; j < size; ++j)
        {
            *GET_COLUMN(columns, j, i, size) = _mm512_loadu_pd(GET_VAL(src, i * doubles_in_vec, j, size));
        }
    }

    memset(U, 0, SQUARE_DOUBLE_MATRIX(size));

    for (size_t i = 0; i < size; ++i)
    {
        *GET_VAL(U, i, i, size) = 1.0;
        *GET_VAL(L, 0, i, size) = *GET_VAL(src, 0, i, size);
    }

    for (size_t i = 0; i < size; ++i)
    {
        for (size_t j = i + 1; j < size; ++j)
        {
            double k = *GET_VAL(L, i, j, size) / *GET_VAL(L, i, i, size);
            __m512d kv = _mm512_set1_pd(k);
            *GET_VAL(U, i, j, size) = k;

            for (size_t col = 0; col < n; ++col)
            {
                *GET_COLUMN(columns, j, col, size) = _mm512_fnmadd_pd(kv, *GET_COLUMN(columns, i, col, size), *GET_COLUMN(columns, j, col, size));
                _mm512_storeu_pd(GET_VAL(L, doubles_in_vec * col, j, size), *GET_COLUMN(columns, j, col, size));
            }
        }
        for (size_t col = 0; col < n; ++col)
        {
            _mm512_storeu_pd(GET_VAL(L, doubles_in_vec * col, i, size), *GET_COLUMN(columns, i, col, size));
        }
    }
    free(columns);
}

void matrix_double_lup(double_matrix_t src, double_matrix_t L, double_matrix_t U, size_t *P, size_t size)
{
    size_t n = size / doubles_in_vec;
    __m512d *columns = malloc(n * size * sizeof(__m512d));
    memset(U, 0, SQUARE_DOUBLE_MATRIX(size));
    for (size_t i = 0; i < size; ++i)
    {
        P[i] = i;
        *GET_VAL(L, 0, i, size) = *GET_VAL(src, 0, i, size);
    }
    for (size_t i = 0; i < n; ++i)
    {
        for (size_t j = 0; j < size; ++j)
        {
            *GET_COLUMN(columns, j, i, size) = _mm512_loadu_pd(GET_VAL(src, i * doubles_in_vec, j, size));
        }
    }

    double *column = malloc(sizeof(double) * size);
    for (size_t i = 0; i < size; ++i)
    {
        size_t maxi = i;
        double max = fabs(*GET_VAL(L, i, i, size));
        for (size_t j = i + 1; j < size; ++j)
        {
            double k = fabs(*GET_VAL(L, i, j, size));
            if (k > max)
            {
                max = k;
                maxi = j;
            }
        }

        if (maxi != i)
        {
            size_t zz = P[maxi];
            P[maxi] = P[i];
            P[i] = zz;

            memcpy(column, GET_VAL(L, 0, i, size), size * sizeof(double));
            memcpy(GET_VAL(L, 0, i, size), GET_VAL(L, 0, maxi, size), size * sizeof(double));
            memcpy(GET_VAL(L, 0, maxi, size), column, size * sizeof(double));

            memcpy(column, GET_VAL(U, 0, i, size), size * sizeof(double));
            memcpy(GET_VAL(U, 0, i, size), GET_VAL(U, 0, maxi, size), size * sizeof(double));
            memcpy(GET_VAL(U, 0, maxi, size), column, size * sizeof(double));

            for (size_t col = 0; col < n; ++col)
            {
                __m512d z = *GET_COLUMN(columns, maxi, col, size);
                *GET_COLUMN(columns, maxi, col, size) = *GET_COLUMN(columns, i, col, size);
                *GET_COLUMN(columns, i, col, size) = z;
            }
        }

        for (size_t j = i + 1; j < size; ++j)
        {
            double k = *GET_VAL(L, i, j, size) / *GET_VAL(L, i, i, size);
            __m512d kv = _mm512_set1_pd(k);
            *GET_VAL(U, i, j, size) = k;

            for (size_t col = 0; col < n; ++col)
            {
                *GET_COLUMN(columns, j, col, size) = _mm512_fnmadd_pd(kv, *GET_COLUMN(columns, i, col, size), *GET_COLUMN(columns, j, col, size));
                _mm512_storeu_pd(GET_VAL(L, doubles_in_vec * col, j, size), *GET_COLUMN(columns, j, col, size));
            }
        }
        for (size_t col = 0; col < n; ++col)
        {
            _mm512_storeu_pd(GET_VAL(L, doubles_in_vec * col, i, size), *GET_COLUMN(columns, i, col, size));
        }
    }
    for (size_t i = 0; i < size; ++i)
    {
        *GET_VAL(U, i, i, size) = 1.0;
    }
    free(column);
    free(columns);
}

void matrix_double_inverse(double_matrix_t src, double_matrix_t dest, size_t size)
{
    size_t n = size / doubles_in_vec;
    __m512d *columns1 = malloc(n * size * sizeof(__m512d));
    __m512d *columns2 = malloc(n * size * sizeof(__m512d));
    memset(dest, 0, SQUARE_DOUBLE_MATRIX(size));
    for (size_t i = 0; i < size; ++i)
    {
        *GET_VAL(dest, i, i, size) = 1.0;
    }
    for (size_t i = 0; i < n; ++i)
    {
        for (size_t j = 0; j < size; ++j)
        {
            *GET_COLUMN(columns1, j, i, size) = _mm512_loadu_pd(GET_VAL(src, i * doubles_in_vec, j, size));
            *GET_COLUMN(columns2, j, i, size) = _mm512_loadu_pd(GET_VAL(dest, i * doubles_in_vec, j, size));
        }
    }
    for (size_t i = 0; i < size; ++i)
    {
        _mm512_storeu_pd(GET_VAL(dest, 0, i, size), *GET_COLUMN(columns1, i, 0, 0));
    }

    double *column = malloc(sizeof(double) * size);
    for (size_t i = 0; i < size; ++i)
    {
        size_t maxi = i;
        double max = fabs(*GET_VAL(dest, i, i, size));
        for (size_t j = i + 1; j < size; ++j)
        {
            double k = fabs(*GET_VAL(dest, i, j, size));
            if (k > max)
            {
                max = k;
                maxi = j;
            }
        }

        if (maxi != i)
        {
            memcpy(column, GET_VAL(dest, 0, i, size), size * sizeof(double));
            memcpy(GET_VAL(dest, 0, i, size), GET_VAL(dest, 0, maxi, size), size * sizeof(double));
            memcpy(GET_VAL(dest, 0, maxi, size), column, size * sizeof(double));

            for (size_t col = 0; col < n; ++col)
            {
                __m512d z = *GET_COLUMN(columns1, maxi, col, size);
                *GET_COLUMN(columns1, maxi, col, size) = *GET_COLUMN(columns1, i, col, size);
                *GET_COLUMN(columns1, i, col, size) = z;

                z = *GET_COLUMN(columns2, maxi, col, size);
                *GET_COLUMN(columns2, maxi, col, size) = *GET_COLUMN(columns2, i, col, size);
                *GET_COLUMN(columns2, i, col, size) = z;
            }
        }

        double k = *GET_VAL(dest, i, i, size);
        __m512d kv = _mm512_set1_pd(k);
        for (size_t col = 0; col < n; ++col)
        {
            *GET_COLUMN(columns1, i, col, size) = _mm512_div_pd(*GET_COLUMN(columns1, i, col, size), kv);
            *GET_COLUMN(columns2, i, col, size) = _mm512_div_pd(*GET_COLUMN(columns2, i, col, size), kv);
            _mm512_storeu_pd(GET_VAL(dest, doubles_in_vec * col, i, size), *GET_COLUMN(columns1, i, col, size));
        }

        for (size_t j = 0; j < size; ++j)
        {
            if (i != j)
            {
                k = *GET_VAL(dest, i, j, size);
                kv = _mm512_set1_pd(k);

                for (size_t col = 0; col < n; ++col)
                {
                    *GET_COLUMN(columns1, j, col, size) = _mm512_fnmadd_pd(kv, *GET_COLUMN(columns1, i, col, size), *GET_COLUMN(columns1, j, col, size));
                    *GET_COLUMN(columns2, j, col, size) = _mm512_fnmadd_pd(kv, *GET_COLUMN(columns2, i, col, size), *GET_COLUMN(columns2, j, col, size));
                    _mm512_storeu_pd(GET_VAL(dest, doubles_in_vec * col, j, size), *GET_COLUMN(columns1, j, col, size));
                }
            }
        }
    }
    for (size_t i = 0; i < n; ++i)
    {
        for (size_t j = 0; j < size; ++j)
        {
            _mm512_storeu_pd(GET_VAL(dest, i * doubles_in_vec, j, size), *GET_COLUMN(columns2, j, i, size));
        }
    }
    free(column);
    free(columns1);
    free(columns2);
}

double matrix_double_determinant(double_matrix_t src, size_t size)
{
    size_t n = size / doubles_in_vec;
    __m512d *columns = malloc(n * size * sizeof(__m512d));
    double_matrix_t L = malloc(SQUARE_DOUBLE_MATRIX(size));
    double res = 1.0;
    for (size_t i = 0; i < size; ++i)
    {
        *GET_VAL(L, 0, i, size) = *GET_VAL(src, 0, i, size);
    }
    for (size_t i = 0; i < n; ++i)
    {
        for (size_t j = 0; j < size; ++j)
        {
            *GET_COLUMN(columns, j, i, size) = _mm512_loadu_pd(GET_VAL(src, i * doubles_in_vec, j, size));
        }
    }

    double *column = malloc(sizeof(double) * size);
    for (size_t i = 0; i < size; ++i)
    {
        size_t maxi = i;
        double max = fabs(*GET_VAL(L, i, i, size));
        for (size_t j = i + 1; j < size; ++j)
        {
            double k = fabs(*GET_VAL(L, i, j, size));
            if (k < max && k > 0.0)
            {
                max = k;
                maxi = j;
            }
        }

        if (maxi != i)
        {
            res *= -1.0;

            memcpy(column, GET_VAL(L, 0, i, size), size * sizeof(double));
            memcpy(GET_VAL(L, 0, i, size), GET_VAL(L, 0, maxi, size), size * sizeof(double));
            memcpy(GET_VAL(L, 0, maxi, size), column, size * sizeof(double));

            for (size_t col = 0; col < n; ++col)
            {
                __m512d z = *GET_COLUMN(columns, maxi, col, size);
                *GET_COLUMN(columns, maxi, col, size) = *GET_COLUMN(columns, i, col, size);
                *GET_COLUMN(columns, i, col, size) = z;
            }
        }

        for (size_t j = i + 1; j < size; ++j)
        {
            double k = *GET_VAL(L, i, j, size) / *GET_VAL(L, i, i, size);
            __m512d kv = _mm512_set1_pd(k);

            for (size_t col = 0; col < n; ++col)
            {
                *GET_COLUMN(columns, j, col, size) = _mm512_fnmadd_pd(kv, *GET_COLUMN(columns, i, col, size), *GET_COLUMN(columns, j, col, size));
                _mm512_storeu_pd(GET_VAL(L, doubles_in_vec * col, j, size), *GET_COLUMN(columns, j, col, size));
            }
        }
        for (size_t col = 0; col < n; ++col)
        {
            _mm512_storeu_pd(GET_VAL(L, doubles_in_vec * col, i, size), *GET_COLUMN(columns, i, col, size));
        }
        res *= *GET_VAL(L, i, i, size);
    }
    free(column);
    free(columns);
    free(L);
    return res;
}

void matrix_double_restore(double_matrix_t src, size_t *P, double_matrix_t dest, size_t size)
{
    for (size_t i = 0; i < size; ++i)
    {
        memcpy(GET_VAL(dest, 0, P[i], size), GET_VAL(src, 0, i, size), size * sizeof(double));
    }
}

void matrix_double_apply(double_matrix_t src, size_t *P, double_matrix_t dest, size_t size)
{
    for (size_t i = 0; i < size; ++i)
    {
        memcpy(GET_VAL(dest, 0, i, size), GET_VAL(src, 0, P[i], size), size * sizeof(double));
    }
}

void matrix_double_mul(double_matrix_t left, size_t r1, size_t c1,
                       double_matrix_t right, size_t r2, size_t c2,
                       double_matrix_t dest)
{
    if (c1 != r2)
    {
        exit(-1);
    }
    size_t n = r1 / doubles_in_vec;
    __m512d *columns = malloc(n * c1 * sizeof(__m512d));
    __m512d curr;
    for (size_t i = 0; i < n; ++i)
    {
        for (size_t j = 0; j < c1; ++j)
        {
            *GET_COLUMN(columns, j, i, c1) = _mm512_loadu_pd(GET_VAL(left, i * doubles_in_vec, j, r1));
        }
    }

    for (size_t c = 0; c < n; ++c)
    {
        for (size_t i = 0; i < c2; ++i)
        {
            curr = _mm512_setzero_pd();
            for (size_t j = 0; j < c1; ++j)
            {
                curr = _mm512_fmadd_pd(_mm512_set1_pd(*GET_VAL(right, j, i, r2)), *GET_COLUMN(columns, j, c, c1), curr);
            }
            _mm512_storeu_pd(GET_VAL(dest, c * doubles_in_vec, i, r1), curr);
        }
    }
    free(columns);
}
