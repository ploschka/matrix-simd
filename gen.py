import numpy as np
import sys
from datetime import datetime
import math
import struct


def output_binary(stream, matrix: np.ndarray):
    if matrix.ndim != 2:
        raise "Not a matrix"

    row, col = matrix.shape
    arr = bytearray()
    for j in range(col):
        for i in range(row):
            arr += bytearray(matrix[i, j])
    stream.write(arr)


def output_text(stream, matrix: np.ndarray):
    if matrix.ndim != 2:
        raise "Not a matrix"

    row, col = matrix.shape

    for j in range(col):
        for i in range(row):
            stream.write(f"{matrix[i, j]}, ")


def is_invertible_arr(input_arr: np.ndarray, index: int) -> bool:
    if input_arr.ndim != 1:
        raise "Not an array"

    d = abs(input_arr[index])
    s = sum(map(abs, input_arr)) - d
    if d < s:
        return False

    return True


def make_invertible_arr(input_arr: np.ndarray, index: int) -> np.ndarray:
    if input_arr.ndim != 1:
        raise "Not an array"

    a = np.array(list(map(abs, input_arr)))
    m = a.max()
    maxi = int(np.where(a == m)[0][0])
    sm = input_arr[maxi]

    input_arr[index], input_arr[maxi] = input_arr[maxi], input_arr[index]

    ret = np.array([(x / m) for x in input_arr])
    ret[index] = sm
    return ret


def coolprint(matrix: np.ndarray):
    if matrix.ndim != 2:
        raise "Not a matrix"

    row, col = matrix.shape

    for i in range(row):
        for j in range(col):
            print(f"{matrix[i, j]:.3f} ", end=' ')
        print()


def gen_mtx(size: int):
    arr = []
    for i in range(size):
        end = False
        while not end:
            print(f"{size} {i}")

            m = 2 * np.random.randn(size)

            mm = make_invertible_arr(m, i)

            end = True
        arr.append(mm)

    ret = np.row_stack(arr)
    np.random.shuffle(ret)
    return ret


def gen_input(size_rate: int, max_size: int, file):
    q = 0
    for i in range(size_rate, max_size, size_rate):
        q += 1
    file.write(struct.pack("@Q", q))

    for i in range(size_rate, max_size, size_rate):
        file.write(struct.pack("@Q", i))

    for i in range(size_rate, max_size, size_rate):
        mtx1 = gen_mtx(i)
        mtx2 = gen_mtx(i)

        sum_m = mtx1 + mtx2
        mul_m = mtx1 @ mtx2
        inv_m = np.linalg.inv(mtx1)
        det = np.linalg.det(mtx1)

        output_binary(file, mtx1)
        output_binary(file, mtx2)

        output_binary(file, sum_m)
        output_binary(file, mul_m)
        output_binary(file, inv_m)
        f.write(struct.pack("@d", det))

if __name__ == "__main__":
    with open("test.bin", "wb") as f:
        np.random.seed(int(datetime.now().timestamp()))
        gen_input(8, 1024, f)
