#ifndef MATRIX_DEF
#define MATRIX_DEF
#ifdef __cplusplus
extern "C"
{
#endif // __cplusplus

#include <stddef.h>
typedef double *double_matrix_t;
typedef const double *const_double_matrix_t;

void matrix_double_print(const_double_matrix_t m, size_t r, size_t c);
#ifndef __cplusplus
#include <stdbool.h>
#endif
bool floats_close_within_(double l, double r, double ulps);
int floats_close_within(double l, double r, double ulps);
void transpose_permutation(size_t *new_p, const size_t *p, size_t size);

#ifdef __cplusplus
}
#endif // __cplusplus
#endif // MATRIX_DEF
