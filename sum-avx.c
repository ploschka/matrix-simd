#include <immintrin.h>

#define VECTOR_WIDTH (sizeof(__m256) / sizeof(float))

void add_matrix_avx(float *dest, const float *left_columns, const float *right_columns, size_t rows, size_t columns)
{
    size_t c = ((rows * columns) / VECTOR_WIDTH);
    for (size_t i = 0; i < c; ++i)
    {
        __m256 left = _mm256_loadu_ps(left_columns + i * VECTOR_WIDTH);
        __m256 right = _mm256_loadu_ps(right_columns + i * VECTOR_WIDTH);
        _mm256_storeu_ps(dest + i * VECTOR_WIDTH, _mm256_add_ps(left, right));
    }
}

