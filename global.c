#include <stdio.h>
#include "global.h"

#define GET_VAL(m, r, c, row_c) (((m) + (r) + (c) * (row_c)))

void matrix_double_print(const_double_matrix_t m, size_t r, size_t c)
{
    for (size_t i = 0; i < r; ++i)
    {
        for (size_t j = 0; j < c; ++j)
        {
            printf("%.3f ", *GET_VAL(m, i, j, r));
        }
        printf("\n");
    }
    printf("\n");
}

#include <float.h>
#include <math.h>
#include <stdbool.h>

bool floats_close_within_(double l, double r, double ulps)
{
    double ulp;
    if (l == r)
        return true;
    if (!l)
        return fabs(r) < DBL_EPSILON * ulps;
    else if (!r)
        return fabs(l) < DBL_EPSILON * ulps;
    if (signbit(l) != signbit(r))
        return false;
    l = fabs(l), r = fabs(r);
    if (r < l)
    {
        double t = l;
        l = r;
        r = t;
    }
    ulp = r - nextafter(r, l);
    // diff = r - l;
    /*if (r - l > ulps * ulp) {
        fprintf(stderr, "Mismatch of flt values %a and %a with difference %a=%aulp greater than %a=%aulp where ulp equals %a\n", l, r, diff, diff/ulp, ulps * ulp, ulps, ulp);
    }*/
    return r - l <= ulps * ulp;
}

int floats_close_within(double l, double r, double ulps)
{
    double ulp;
    if (l == r)
        return 1;
    if (!l)
        return fabs(r) < DBL_EPSILON * ulps;
    else if (!r)
        return fabs(l) < DBL_EPSILON * ulps;
    if (signbit(l) != signbit(r))
        return -1;
    l = fabs(l), r = fabs(r);
    if (r < l)
    {
        double t = l;
        l = r;
        r = t;
    }
    ulp = r - nextafter(r, l);
    // diff = r - l;
    /*if (r - l > ulps * ulp) {
        fprintf(stderr, "Mismatch of flt values %a and %a with difference %a=%aulp greater than %a=%aulp where ulp equals %a\n", l, r, diff, diff/ulp, ulps * ulp, ulps, ulp);
    }*/
    return r - l <= ulps * ulp;
}

void transpose_permutation(size_t *new_p, const size_t *p, size_t size)
{
    for (size_t i = 0; i < size; ++i)
    {
        for (size_t j = 0; j < size; ++j)
        {
            if (i == p[j])
            {
                new_p[i] = j;
                break;
            }
        }
    }
}
