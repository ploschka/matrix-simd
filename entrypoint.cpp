#include <cstring>
#include <cstdlib>
#include <algorithm>
#include <ranges>
#include <vector>
#include <chrono>
#include <iostream>

#include "avx512.h"
#include "avx2.h"
#include "global.h"

int main()
{
	std::cout.sync_with_stdio();
	{
		using std::size;
		double x[][8] = {
			{  1,   3,   7,  11,  13,  17,  19,  23},
			{  1,   3,   7,  11,  13,  17,  19,  23},
			{ 57,  59,  61,  67,  71,  73,  79,  83},
			{ 89,  97, 101, 103, 107, 109, 113, 127},
			{131, 137, 139, 149, 151, 157, 163, 167},
			{179, 181, 191, 193, 197, 199, 211, 223},
			{227, 229, 233, 239, 241, 251, 257, 263},
			{269, 271, 277, 281, 283, 293, 307, 311}
		},
		check[][8] = {
			{ 16628,  16934,  17418,  17868,  18126,  18710,  19522,  20182},
			{ 16628,  16934,  17418,  17868,  18126,  18710,  19522,  20182},
			{ 72184,  73970,  76510,  79136,  80638,  83550,  87354,  90966},
			{108452, 111190, 115066, 119096, 121414, 125826, 131622, 137134},
			{151260, 155146, 160618, 166292, 169570, 175738, 183810, 191658},
			{197736, 202830, 209990, 217488, 221802, 229930, 240526, 250766},
			{240594, 246872, 255716, 264922, 270248, 280164, 293112, 305784},
			{284206, 291608, 302036, 312922, 319208, 330948, 346224, 361160}
		}, z[size(x)][size(x[0])];
		std::cout << "Source matrix:\n";
		matrix_double_print(&x[0][0], size(x), size(x));
		matrix_avx512_double_mul(&x[0][0], size(x), size(x[0]), &x[0][0], size(x), size(x[0]), &z[0][0]);
		std::cout << "Product matrix:\n";
		matrix_double_print(&z[0][0], size(z), size(z[0]));
		auto [i1, i2] = std::mismatch(&z[0][0], &z[8][0], &check[0][0], &check[8][0]);
		if (i1 != &z[8][0]) {
			std::cerr << "Mismatch at " << i1 - &z[0][0] << " of " << *i1 << " and " << *i2 << '\n';
			return __LINE__;
		}
	}
	{
		double src[] = {
			  1,   3,   7,  11,  13,  17,  19,  23,
			 29,  31,  37,  41,  43,  47,  51,  53,
			 57,  59,  61,  67,  71,  73,  79,  83,
			 89,  97, 101, 103, 107, 109, 113, 127,
			131, 137, 139, 149, 151, 157, 163, 167,
			179, 181, 191, 193, 197, 199, 211, 223,
			227, 229, 233, 239, 241, 251, 257, 263,
			269, 271, 277, 281, 283, 293, 307, 311
		}, dest[sizeof src/sizeof src[0]];
		matrix_avx512_double_gaussian(src, dest, 8);
		std::cout << "Source matrix:\n";
		matrix_double_print(src, 8, 8);
		std::cout << "Decomposed matrix:\n";
		matrix_double_print(dest, 8, 8);
	}
	{
		using std::size;
		double src[][8] = {
			{  1,   3,   7,  11,  13,  17,  19,  23},
			{ 29,  31,  37,  41,  43,  47,  51,  53},
			{ 57,  59,  61,  67,  71,  73,  79,  83},
			{ 89,  97, 101, 103, 107, 109, 113, 127},
			{131, 137, 139, 149, 151, 157, 163, 167},
			{179, 181, 191, 193, 197, 199, 211, 223},
			{227, 229, 233, 239, 241, 251, 257, 263},
			{269, 271, 277, 281, 283, 293, 307, 311}
		}, L[size(src)][size(src[0])], U[size(src)][size(src[0])], check[size(src)][size(src[0])];
		size_t P[size(src)];
		matrix_avx512_double_lup(&src[0][0], &L[0][0], &U[0][0], P, size(src));
		std::cout << "Source matrix:\n";
		matrix_double_print(&src[0][0], size(src), size(src));
		std::cout << "L matrix:\n";
		matrix_double_print(&L[0][0], size(L), size(L));
		std::cout << "U matrix:\n";
		matrix_double_print(&U[0][0], size(U), size(U));
		std::cout << "Permutation:\n";
		for (auto p:P)
			std::cout << p << ' ';
		std::cout << '\n';
		matrix_avx512_double_mul(&L[0][0], size(L), size(L[0]), &U[0][0], size(U), size(U[0]), &check[0][0]);
		matrix_avx512_permute_columns(&L[0][0], P, size(src));
		matrix_double_print(&check[0][0], size(check), size(check[0]));
		auto [i1, i2] = std::mismatch(&check[0][0], &check[8][0], &src[0][0], &src[8][0], [](auto l, auto r) {return floats_close_within(l, r, 64);});
		if (i1 != &check[8][0]) {
			std::cerr << "Mismatch at " << i1 - &check[0][0] << " of " << *i1 << " and " << *i2 << '\n';
			return __LINE__;
		}
	}
	{
		using std::size;
		double src[][8] = {
			{  1,   3,   7,  11,  13,  17,  19,  23},
			{  1,   3,   7,  11,  13,  17,  19,  23},
			{ 57,  59,  61,  67,  71,  73,  79,  83},
			{ 89,  97, 101, 103, 107, 109, 113, 127},
			{131, 137, 139, 149, 151, 157, 163, 167},
			{179, 181, 191, 193, 197, 199, 211, 223},
			{227, 229, 233, 239, 241, 251, 257, 263},
			{269, 271, 277, 281, 283, 293, 307, 311}
		}, L[size(src)][size(src[0])], U[size(src)][size(src[0])], check[size(src)][size(src[0])];
		size_t P[size(src)];
		matrix_avx512_double_lup(&src[0][0], &L[0][0], &U[0][0], P, size(src));
		std::cout << "Source matrix:\n";
		matrix_double_print(&src[0][0], size(src), size(src));
		std::cout << "L matrix:\n";
		matrix_double_print(&L[0][0], size(L), size(L));
		std::cout << "U matrix:\n";
		matrix_double_print(&U[0][0], size(U), size(U));
		std::cout << "Permutation:\n";
		for (auto p:P)
			std::cout << p << ' ';
		std::cout << '\n';
		matrix_avx512_double_mul(&L[0][0], size(L), size(L[0]), &U[0][0], size(U), size(U[0]), &check[0][0]);
		std::cout << "Before permutation:\n";
		matrix_double_print(&check[0][0], size(check), size(check[0]));
		matrix_avx512_permute_columns(&check[0][0], P, size(src));
		std::cout << "After permutation:\n";
		matrix_double_print(&check[0][0], size(check), size(check[0]));
	}/*
	{
		using std::size;
		double x[][8] = {
			{  1,   3,   7,  11,  13,  17,  19,  23},
			{  1,   3,   7,  11,  13,  17,  19,  23},
			{ 57,  59,  61,  67,  71,  73,  79,  83},
			{ 89,  97, 101, 103, 107, 109, 113, 127},
			{131, 137, 139, 149, 151, 157, 163, 167},
			{179, 181, 191, 193, 197, 199, 211, 223},
			{227, 229, 233, 239, 241, 251, 257, 263},
			{269, 271, 277, 281, 283, 293, 307, 311}
		}, y[][8] = {
			{0, 0, 0, 1, 0, 0, 0, 0},
			{1, 0, 0, 0, 0, 0, 0, 0},
			{0, 0, 0, 0, 0, 0, 0, 1},
			{0, 0, 0, 0, 0, 1, 0, 0},
			{0, 0, 0, 0, 0, 0, 1, 0},
			{0, 0, 1, 0, 0, 0, 0, 0},
			{0, 0, 0, 0, 1, 0, 0, 0},
			{0, 1, 0, 0, 0, 0, 0, 0}
		}, 
		z[size(x)][size(x[0])];
		std::cout << "Source matrix:\n";
		matrix_double_print(&x[0][0], size(x), size(x));
		matrix_avx512_double_mul(&x[0][0], size(x), size(x[0]), &y[0][0], size(y), size(y[0]), &z[0][0]);
		matrix_double_print(&z[0][0], size(z), size(z[0]));
	}*/
	/*{
		const size_t n = 8;
		double src[n * n], dest[n * n];
		for (std::size_t r = 0; r < n; ++r)
			for (std::size_t c = 0; c < n; ++c)
				src[r + c * n] = r + c * sizeof(src[0])+1;
		matrix_avx512_double_gaussian(src, dest, n);
		std::cout << "Source matrix:\n";
		matrix_double_print(src, n, n);
		std::cout << "Decomposed matrix:\n";
		matrix_double_print(dest, n, n);
	}
	{
		double a[16], b[16], c[16];
		std::ranges::fill(a, 1.1);
		std::ranges::fill(b, 2.2);
		auto test = [&a, &b, &c](auto fn)
		{
			fn(c, a, b, sizeof a / sizeof a[0], 1);
			return std::ranges::find_if(c, [](auto e)
										{ return e != 3.3; }) == c;
		};
		if (!test(matrix_avx512_double_sum))
			return __LINE__;
		if (!test(matrix_avx2_double_sum))
			return __LINE__;
	}*/
	/*{
		using namespace std::chrono;
		std::size_t n = 1u << 10;
		auto a = std::vector<double>(n*n, 1.1f), b = std::vector<double>(a.size(), 2.2f), c = std::vector<double>(a.size());
		auto tm1 = steady_clock::now();
		matrix_avx2_double_mul(a.data(), n, n, b.data(), n, n, c.data());
		auto tm2 = steady_clock::now();
		matrix_avx512_double_mul(a.data(), n, n, b.data(), n, n, c.data());
		auto tm3 = steady_clock::now();
		std::cout << "AVX time: " << duration_cast<milliseconds>(tm2 - tm1).count() << '\n';
		std::cout << "AVX-512 time: " << duration_cast<milliseconds>(tm3 - tm2).count() << '\n';
	}*/
	{
		using namespace std::chrono;
		std::size_t n = 1u << 10;
		auto a = std::vector<double>(n*n, 1.1f), l = std::vector<double>(a.size()), u = std::vector<double>(a.size());
		auto p = std::vector<std::size_t>(n);
		srand(0);
		std::ranges::generate(a, rand);
		auto tm1 = steady_clock::now();
		matrix_avx2_double_lup(a.data(), l.data(), u.data(), p.data(), n);
		auto tm2 = steady_clock::now();
		matrix_avx512_double_lup(a.data(), l.data(), u.data(), p.data(), n);
		auto tm3 = steady_clock::now();
		std::cout << "AVX time: " << duration_cast<milliseconds>(tm2 - tm1).count() << '\n';
		std::cout << "AVX-512 time: " << duration_cast<milliseconds>(tm3 - tm2).count() << '\n';
	}
	return 0;
}

