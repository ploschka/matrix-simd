#ifndef MATRIX_AVX512
#define MATRIX_AVX512
#ifdef __cplusplus
extern "C"
{
#endif // __cplusplus

#include "global.h"

int matrix_avx512_double_sum(const_double_matrix_t left, const_double_matrix_t right, double_matrix_t dest, size_t r, size_t c);
int matrix_avx512_double_gaussian(const_double_matrix_t src, double_matrix_t dest, size_t size);
int matrix_avx512_double_lu(const_double_matrix_t src, double_matrix_t L, double_matrix_t U, size_t size);
int matrix_avx512_double_lup(const_double_matrix_t src, double_matrix_t L, double_matrix_t U, size_t *P, size_t size);
int matrix_avx512_double_inverse(const_double_matrix_t src, double_matrix_t dest, size_t size);
int matrix_avx512_double_determinant(const_double_matrix_t src, double *res, size_t size);
int matrix_avx512_double_apply(const_double_matrix_t src, const size_t *P, double_matrix_t dest, size_t size);
int matrix_avx512_double_mul(const_double_matrix_t left, size_t r1, size_t c1,
                             const_double_matrix_t right, size_t r2, size_t c2,
                             double_matrix_t dest);
int matrix_avx512_double_mul_unlocal(const_double_matrix_t left, size_t r1, size_t c1,
                                     const_double_matrix_t right, size_t r2, size_t c2,
                                     double_matrix_t dest);

void matrix_avx512_permute_columns(double *m, const size_t *p, size_t n);

#ifdef __cplusplus
}
#endif // __cplusplus
#endif // MATRIX_AVX512
