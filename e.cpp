#include <cstdlib>
#include <chrono>
#include <sys/mman.h>
#include <fcntl.h>
#include <unistd.h>
#include <cstring>
#include <cmath>
#include <limits>
#include <ranges>
#include <algorithm>
#include <utility>
#include <cerrno>

#include "e.hpp"
#include "avx512.h"

#ifdef NDEBUG
static void dbg_print(auto &&...x) {}
#else
static void dbg_print(auto &&...x) { fprintf(stderr, x...); }
#endif

FILE *file_prep(const char *name)
{
    auto file = fopen(name, "w");
    if (!file)
    {
        fprintf(stderr, "Could not open %s.\n", name);
        exit(-1);
    }
    fprintf(file, "name,time,ulp,size\n");
    return file;
}

static const double max_ulp = 10000.0;
static double vec_err_(const std::vector<double> &vec, const_double_matrix_t example, size_t size)
{
    double ulp = 0.0;

    auto i1 = vec.cbegin();
    auto i2 = example;

    for (size_t i = 0; i < size; ++i, ++i1, ++i2)
    {
        bool end = false;
        while (!end)
        {
            auto res = floats_close_within(*i1, *i2, ulp);
            switch (res)
            {
            case 1:
                end = true;
                break;
            case 0:
                dbg_print("sequences mismatch at offset %u (%a and %a) ulp = %.3f\n", i1 - vec.cbegin(), *i1, *i2, ulp);
                ulp += 1.0;
                break;
            case -1:
                dbg_print("sequences mismatch at offset %u (%a and %a) ulp = %.3f\n", i1 - vec.cbegin(), *i1, *i2, ulp);
                return -1.0 * max_ulp;
                break;
            default:
                fprintf(stderr, "Unknown error at %s line %d\n", __FILE__, __LINE__);
                exit(-1);
            }
            // if (ulp >= max_ulp)
            //     return ulp;
        }
    }
    return ulp;
}

static double get_err(double first, double second, double start_ulp)
{
    double ulp = start_ulp;
    for (;; ulp += 1.0)
    {
        auto res = floats_close_within(first, second, ulp);
        switch (res)
        {
        case 1:
            dbg_print("returned %.0f\n", ulp);
            return ulp;
            break;
        case 0:
            dbg_print("mismatch (%a and %a) ulp = %.0f\n", first, second, ulp);
            break;
        case -1:
            return -1.0 * max_ulp;
            break;
        default:
            fprintf(stderr, "Unknown error at %s line %d\n", __FILE__, __LINE__);
            exit(-1);
        }
    }
    dbg_print("returned %.0f\n", ulp);
    return ulp;
}

static double vec_err(const std::vector<double> &vec, const_double_matrix_t example, size_t size)
{
    double ret_ulp = 0.0;
    double prev_ulp = 0.0;
    double ulp = 1.0;

    auto i1 = vec.cbegin();
    auto i2 = example;

    for (size_t i = 0; i < size; ++i, ++i1, ++i2)
    {
        bool end = false;
        while (!end)
        {
            auto res = floats_close_within(*i1, *i2, ulp);
            double rr;
            switch (res)
            {
            case 1:
                dbg_print("sequences match at offset %u (%a and %a) ulp = %.0f\n", i1 - vec.cbegin(), *i1, *i2, ulp);
                end = true;
                rr = get_err(*i1, *i2, prev_ulp);
                if (rr < 0.0)
                {
                    return -1.0 * max_ulp;
                }
                if (rr > ret_ulp)
                {
                    ret_ulp = rr;
                }
                break;
            case 0:
                dbg_print("sequences mismatch at offset %u (%a and %a) ulp = %.0f\n", i1 - vec.cbegin(), *i1, *i2, ulp);
                prev_ulp = ulp;
                ulp *= 2.0;
                break;
            case -1:
                dbg_print("sequences mismatch at offset %u (%a and %a) ulp = %.0f\n", i1 - vec.cbegin(), *i1, *i2, ulp);
                return -1.0 * max_ulp;
                break;
            default:
                fprintf(stderr, "Unknown error at %s line %d\n", __FILE__, __LINE__);
                exit(-1);
            }
        }
    }
    dbg_print("set ulp = %.0f\n", ulp);
    return ret_ulp;
}

static double scalar_err(double scalar, double example)
{
    double ret_ulp = 0.0;
    double prev_ulp = 0.0;
    double ulp = 1.0;

    bool end = false;
    while (!end)
    {
        auto res = floats_close_within(scalar, example, ulp);
        double rr;
        switch (res)
        {
        case 1:
            end = true;
            rr = get_err(scalar, example, prev_ulp);
            if (rr < 0.0)
            {
                return -1.0 * max_ulp;
            }
            if (rr > ret_ulp)
            {
                ret_ulp = rr;
            }
            break;
        case 0:
            dbg_print("scalar mismatch (%a and %a) ulp = %.0f\n", scalar, example, ulp);
            prev_ulp = ulp;
            ulp *= 2.0;
            break;
        case -1:
            dbg_print("scalar mismatch (%a and %a) ulp = %.0f\n", scalar, example, ulp);
            return -1.0 * max_ulp;
            break;
        default:
            fprintf(stderr, "Unknown error at %s line %d\n", __FILE__, __LINE__);
            exit(-1);
        }
    }
    return ret_ulp;
}

static bool vec_eq(const std::vector<double> &vec1, const std::vector<double> &vec2)
{
    auto [i1, i2] = std::ranges::mismatch(vec1, vec2, [](double l, double r)
                                          { return floats_close_within(l, r, 5); });
    if (i1 != vec1.end() || i2 != vec2.end())
    {
        dbg_print("sequences mismatch at offset %u (%a and %a)\n", i1 - vec1.begin(), *i1, *i2);
        return false;
    }
    return true;
}

void rand_init(double_mtx_vec_t &vec1, double_mtx_vec_t &vec2)
{
    size_t size = std::min(vec1.size(), vec2.size());
    for (size_t i = 0; i < size; ++i)
    {
        vec1[i] = rand();
        vec2[i] = rand();
    }
}

void rand_init(double_mtx_vec_t &vec)
{
    size_t size = vec.size();
    for (size_t i = 0; i < size; ++i)
    {
        vec[i] = rand();
    }
}

static void log(FILE *log_out, const char *name, size_t size)
{
    fprintf(log_out, "Running %s on size %lu...\n", name, size);
    fflush(log_out);
}

static double measure_time(std::function<void(size_t)> func, size_t i)
{
    auto t1 = std::chrono::high_resolution_clock::now();
    func(i);
    auto t2 = std::chrono::high_resolution_clock::now();

    return std::chrono::duration_cast<std::chrono::milliseconds>(t2 - t1).count();
}

void get_result(res_vec_t &vec, FILE *log_out, const std::string &name,
                std::function<void(size_t)> func, size_t i,
                const double_mtx_vec_t &res, const_double_matrix_t example)
{
    log(log_out, name.c_str(), i);

    for (size_t j = 0; j < repeat_times; ++j)
    {
        auto &last_res = vec.emplace_back();
        last_res.name = name;
        last_res.size = i;
        last_res.time = measure_time(func, i);
    }
    auto iter = vec.rbegin();
    double err = vec_err(res, example, i);
    for (size_t j = 0; j < repeat_times; ++j)
    {
        iter->ulp = err;
        ++iter;
    }
}

void get_result(res_vec_t &vec, FILE *log_out, const std::string &name,
                std::function<void(size_t)> func, size_t i,
                double &res, double example)
{
    log(log_out, name.c_str(), i);

    for (size_t j = 0; j < repeat_times; ++j)
    {
        auto &last_res = vec.emplace_back();
        last_res.name = name;
        last_res.size = i;
        last_res.time = measure_time(func, i);
    }
    auto iter = vec.rbegin();
    double err = scalar_err(res, example);
    for (size_t j = 0; j < repeat_times; ++j)
    {
        iter->ulp = err;
        ++iter;
    }
}

void get_result(res_vec_t &vec, FILE *log_out, const std::string &name,
                std::function<void(size_t)> func, size_t size,
                const double_mtx_vec_t &res1, const double_mtx_vec_t &res2,
                const_double_matrix_t example)
{
    log(log_out, name.c_str(), size);

    std::vector<double> gg(size * size);

    for (size_t j = 0; j < repeat_times; ++j)
    {
        auto &last_res = vec.emplace_back();
        last_res.name = name;
        last_res.size = size;
        last_res.time = measure_time(func, size);

        matrix_avx512_double_mul(res1.data(), size, size, res2.data(), size, size, gg.data());
    }
    auto iter = vec.rbegin();
    double err = vec_err(gg, example, size);
    for (size_t j = 0; j < repeat_times; ++j)
    {
        iter->ulp = err;
        ++iter;
    }
}

void get_result(res_vec_t &vec, FILE *log_out, const std::string &name,
                std::function<void(size_t)> func, size_t size,
                const double_mtx_vec_t &res1, const double_mtx_vec_t &res2,
                const std::vector<size_t> resp, const_double_matrix_t example)
{
    log(log_out, name.c_str(), size);

    std::vector<double> gg(size * size);

    for (size_t j = 0; j < repeat_times; ++j)
    {
        auto &last_res = vec.emplace_back();
        last_res.name = name;
        last_res.size = size;
        last_res.time = measure_time(func, size);

        matrix_avx512_double_mul(res1.data(), size, size, res2.data(), size, size, gg.data());
        matrix_avx512_permute_columns(gg.data(), resp.data(), size);
    }
    auto iter = vec.rbegin();
    double err = vec_err(gg, example, size);
    for (size_t j = 0; j < repeat_times; ++j)
    {
        iter->ulp = err;
        ++iter;
    }
}

void write_result(const result_t &res, FILE *file)
{
    fprintf(file, pattern, res.name.c_str(), res.time, res.ulp, res.size);
}

void read_mtx(const char *filename, test_vec_t &vec, mem_t &mem)
{
    int fd = open(filename, O_RDONLY);
    if (fd < 0)
    {
        fprintf(stderr, "Open error\n%s\n", strerror(errno));
        exit(-1);
    }
    off_t len = lseek(fd, 0, SEEK_END);
    if (len < 0)
    {
        fprintf(stderr, "Seek error\n%s\n", strerror(errno));
        exit(-1);
    }
    if (len < sizeof(uint64_t))
    {
        fprintf(stderr, "Invalid size\n");
        exit(-1);
    }
    void *addr = mmap(NULL, len, PROT_READ, MAP_SHARED, fd, 0);
    if (addr == MAP_FAILED)
    {
        fprintf(stderr, "Map error\n%s\n", strerror(errno));
        exit(-1);
    }
    if (close(fd) < 0)
    {
        fprintf(stderr, "Close error\n%s\n", strerror(errno));
        exit(-1);
    }
    mem.len = len;
    mem.addr = addr;

    uint64_t l = *reinterpret_cast<uint64_t *>(addr);

    vec.resize(l);

    void *next = reinterpret_cast<uint64_t *>(addr) + 1ul;

    for (size_t i = 0; i < l; ++i)
    {
        vec[i].size = *reinterpret_cast<uint64_t *>(next);
        next = reinterpret_cast<uint64_t *>(next) + 1ul;
    }

    for (auto &i : vec)
    {
        i.mtx1 = reinterpret_cast<const_double_matrix_t>(next);
        next = reinterpret_cast<double *>(next) + i.size * i.size;

        i.mtx2 = reinterpret_cast<const_double_matrix_t>(next);
        next = reinterpret_cast<double *>(next) + i.size * i.size;

        i.sum = reinterpret_cast<const_double_matrix_t>(next);
        next = reinterpret_cast<double *>(next) + i.size * i.size;

        i.mul = reinterpret_cast<const_double_matrix_t>(next);
        next = reinterpret_cast<double *>(next) + i.size * i.size;

        i.inv = reinterpret_cast<const_double_matrix_t>(next);
        next = reinterpret_cast<double *>(next) + i.size * i.size;

        i.det = *reinterpret_cast<double *>(next);
        next = reinterpret_cast<double *>(next) + 1ul;
    }
}

void close_mtx(mem_t &mem)
{
    if (munmap(mem.addr, mem.len) < 0)
    {
        fprintf(stderr, "Unmap error\n%s\n", strerror(errno));
        exit(-1);
    }
}
